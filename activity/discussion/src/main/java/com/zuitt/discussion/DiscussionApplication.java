package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling Http request

@RestController
@RequestMapping("/greeting")
//will require all routs within the class to use the set endpoint as part of its rout
public class DiscussionApplication {

	ArrayList<String> enrollees = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	Activity S09 Friday deadline

	//Create /enroll route that will accept a query String with a parameter of user;
	@GetMapping("/enroll")
	public String add (@RequestParam(value="user",defaultValue = "genica")String user){
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!",user);
	}

	//Create a "/getEnrollees" route which return the content of enrollees ArrayList as a string
	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){
		return enrollees;
	}


	//Create a "/nameage" route that will accept multiple query string parameter of name and age;
	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name", defaultValue = "genica")String name,@RequestParam(value="age",defaultValue = "22")int age){
		return String.format("Hello %s! My age is %d.",name,age);
	}


	//Create a "/course/id" dynamic rout using a path variable of id
	@GetMapping("/course/{id}")
	public String course(@PathVariable("id")String id){

		switch(id){
			case "java101":
				return String.format("Name: JAVA 101, MWF 8:00AM-11:00AM, PHP 3000.00 ");

			case "sql101":
				return String.format("Name: SQL 101, TTH 1:00PM-4:00PM, PHP 2000.00 ");

			case "javaee101":
				return String.format("Name: JAVA EE 101, MWF 1:00PM-4:00PM, PHP 3500.00 ");

			default:
				return String.format("The course cannot be found!");
		}
	}

}
